module org.refcodes.security.alt.chaos {
	requires org.refcodes.codec;
	requires org.refcodes.data;
	requires org.refcodes.numerical;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.security;
	requires java.logging;
	requires org.refcodes.runtime;

	exports org.refcodes.security.alt.chaos;
}

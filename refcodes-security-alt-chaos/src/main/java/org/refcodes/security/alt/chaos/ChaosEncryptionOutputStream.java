// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import java.io.IOException;
import java.io.OutputStream;
import java.security.SecureRandom;

import org.refcodes.security.EncryptionException;
import org.refcodes.security.EncryptionOutputStream;

/**
 * A {@link ChaosEncryptionOutputStream} wraps an {@link OutputStream} and
 * produces output bytes by applying a {@link ChaosEncrypter} on each byte to be
 * written before delegating the processed vale to the given
 * {@link OutputStream}. The output of the {@link ChaosEncryptionOutputStream}
 * can be converted back by the according {@link ChaosDecryptionInputStream}.
 * 
 * In case salting is enabled, then the {@link ChaosEncryptionOutputStream}
 * initially encrypts heading bytes of an {@link OutputStream} with a random
 * salt {@link ChaosKey} which then is used as initially encrypting the plan
 * data (being reversed using the {@link ChaosDecryptionInputStream} with
 * salting enabled).
 */
public class ChaosEncryptionOutputStream extends EncryptionOutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ChaosEncryptionOutputStream} using the given
	 * {@link ChaosKey} for encrypting.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aKey The {@link ChaosKey} to use for encrypting.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred while
	 *         applying salting.
	 */
	public ChaosEncryptionOutputStream( OutputStream aOutputStream, ChaosKey aKey ) throws IOException {
		this( aOutputStream, aKey, false );
	}

	/**
	 * Constructs the {@link ChaosEncryptionOutputStream} using the given
	 * {@link ChaosKey} for encrypting.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be wrapped.
	 * @param aKey The {@link ChaosKey} to use for encrypting.
	 * @param isVerify When true then the encryption is verified against an
	 *        according live decryption to throw an
	 *        {@link IllegalStateException} in case encryption and decryption
	 *        differ.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred while
	 *         applying salting.
	 */
	public ChaosEncryptionOutputStream( OutputStream aOutputStream, ChaosKey aKey, boolean isVerify ) throws IOException {
		super( toOutputStream( aOutputStream, aKey ), toChaosEncrypter( aOutputStream, aKey, isVerify ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	private static OutputStream toOutputStream( OutputStream aOutputStream, ChaosKey aKey ) throws IOException {
		aKey = aKey.nextVariableLengthChild();
		return aKey == null ? aOutputStream : new ChaosEncryptionOutputStream( aOutputStream, aKey );
	}

	private static ChaosEncrypter toChaosEncrypter( OutputStream aOutputStream, ChaosKey aKey, boolean isVerify ) throws IOException {
		final int theFixedLengthChildDepth = aKey.fixedLengthChildDepth();
		final ChaosEncrypter theEncrypter = new ChaosEncrypter( aKey, theFixedLengthChildDepth, isVerify );
		try {
			// PREFIX  |-->
			if ( aKey.getOptions().hasRndPrefix() ) {
				final SecureRandom rnd = new SecureRandom();
				byte[] thePrefix = new byte[aKey.getOptions().getRndPrefixSize()];
				rnd.nextBytes( thePrefix );
				thePrefix = theEncrypter.toEncrypted( thePrefix );
				aOutputStream.write( thePrefix );
				aOutputStream.flush();
			}
			// PREFIX <--|

			// SALTED |-->
			if ( aKey.getOptions().isSalted() ) {
				final ChaosKey theSaltKey = ChaosKey.createRndKey();
				final byte[] theSalt = theEncrypter.toEncrypted( theSaltKey.getEncoded() );
				aOutputStream.write( theSalt );
				aOutputStream.flush();
				return new ChaosEncrypter( theSaltKey, theFixedLengthChildDepth, theEncrypter, isVerify );
			}
			// SALTED <--|
		}
		catch ( EncryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal sate while encrypting!", e );
		}
		// SALTED <--|
		return theEncrypter;
	}
}

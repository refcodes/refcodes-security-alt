// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

/**
 * The {@link ChaosMode} specifies the mode of operation of a {@link ChaosKey}
 * when using Chaos-based encryption/decryption.
 * 
 * Attention: Comparing a {@link ChaosMode} with a different implementation of
 * the {@link ChaosOptions} only functions as expected when passing the
 * {@link ChaosMode} as argument to that different {@link ChaosOptions}
 * implementation's {@link ChaosOptions#equals(Object)} method as the
 * {@link #equals(Object)} method of the {@link ChaosMode} enumeration cannot be
 * overwritten (the {@link #equals(Object)} method of an enumeration cannot be
 * overridden)!
 */
public enum ChaosMode implements ChaosOptions {

	/**
	 * No additions are to be applied to the raw Chaos-based
	 * encryption/decryption.
	 */
	NONE(false, false, false, (short) 0),

	/**
	 * A MUTATION operation as of {@link #isMutateS()} is being applied.
	 */
	MUTATE(true, false, false, (short) 0),

	/**
	 * XOR obfuscation as of {@link #isXorNext()} is being applied.
	 */
	XOR(false, true, false, (short) 0),

	/**
	 * Combination of the {@link #XOR} mode alongside the {@link #MUTATE} mode.
	 */
	MUTATE_XOR(true, true, false, (short) 0),

	/**
	 * A SALTING operation as of {@link #isSalted()} is being applied where
	 * applicable.
	 */
	SALTED(false, false, true, (short) 0),

	/**
	 * Combination of the {@link #SALTED} mode alongside the {@link #MUTATE}
	 * mode.
	 */
	SALTED_MUTATE(true, false, true, (short) 0),

	/**
	 * Combination of the {@link #SALTED} mode alongside the {@link #XOR} mode.
	 */
	SALTED_XOR(false, true, true, (short) 0),

	/**
	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	 * alongside the {@link #MUTATE} mode.
	 */
	SALTED_MUTATE_XOR(true, true, true, (short) 0);

	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 1 is being
	//	 * applied.
	//	 */
	//	PREFIX1(false, false, false, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX1}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX1(true, false, false, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX1} mode.
	//	 */
	//	XOR_PREFIX1(false, true, false, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX1} mode.
	//	 */
	//	MUTATE_XOR_PREFIX1(true, true, false, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX1}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX1(false, false, true, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX1} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX1(true, false, true, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX1} mode.
	//	 */
	//	SALTED_XOR_PREFIX1(false, true, true, (short) 1),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX1} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX1(true, true, true, (short) 1),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 8 is being
	//	 * applied.
	//	 */
	//	PREFIX8(false, false, false, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX8}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX8(true, false, false, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX8} mode.
	//	 */
	//	XOR_PREFIX8(false, true, false, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX8} mode.
	//	 */
	//	MUTATE_XOR_PREFIX8(true, true, false, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX8}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX8(false, false, true, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX8} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX8(true, false, true, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX8} mode.
	//	 */
	//	SALTED_XOR_PREFIX8(false, true, true, (short) 8),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX8} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX8(true, true, true, (short) 8),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 16 is
	//	 * being applied.
	//	 */
	//	PREFIX16(false, false, false, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX16}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX16(true, false, false, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX16}
	//	 * mode.
	//	 */
	//	XOR_PREFIX16(false, true, false, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX16} mode.
	//	 */
	//	MUTATE_XOR_PREFIX16(true, true, false, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX16}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX16(false, false, true, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX16} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX16(true, false, true, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX16} mode.
	//	 */
	//	SALTED_XOR_PREFIX16(false, true, true, (short) 16),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX16} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX16(true, true, true, (short) 16),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 32 is
	//	 * being applied.
	//	 */
	//	PREFIX32(false, false, false, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX32}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX32(true, false, false, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX32}
	//	 * mode.
	//	 */
	//	XOR_PREFIX32(false, true, false, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX32} mode.
	//	 */
	//	MUTATE_XOR_PREFIX32(true, true, false, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX32}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX32(false, false, true, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX32} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX32(true, false, true, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX32} mode.
	//	 */
	//	SALTED_XOR_PREFIX32(false, true, true, (short) 32),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX32} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX32(true, true, true, (short) 32),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 64 is
	//	 * being applied.
	//	 */
	//	PREFIX64(false, false, false, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX64}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX64(true, false, false, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX64}
	//	 * mode.
	//	 */
	//	XOR_PREFIX64(false, true, false, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX64} mode.
	//	 */
	//	MUTATE_XOR_PREFIX64(true, true, false, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX64}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX64(false, false, true, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX64} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX64(true, false, true, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX64} mode.
	//	 */
	//	SALTED_XOR_PREFIX64(false, true, true, (short) 64),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX64} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX64(true, true, true, (short) 64),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 128 is
	//	 * being applied.
	//	 */
	//	PREFIX128(false, false, false, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX128}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX128(true, false, false, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX128}
	//	 * mode.
	//	 */
	//	XOR_PREFIX128(false, true, false, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX128} mode.
	//	 */
	//	MUTATE_XOR_PREFIX128(true, true, false, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX128}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX128(false, false, true, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX128} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX128(true, false, true, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX128} mode.
	//	 */
	//	SALTED_XOR_PREFIX128(false, true, true, (short) 128),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX128} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX128(true, true, true, (short) 128),
	//
	//	/**
	//	 * A PREFIX operation as of {@link #getRndPrefixSize()} with size 256 is
	//	 * being applied.
	//	 */
	//	PREFIX256(false, false, false, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #MUTATE} mode alongside the {@link #PREFIX256}
	//	 * mode.
	//	 */
	//	MUTATE_PREFIX256(true, false, false, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #XOR} mode alongside the {@link #PREFIX256}
	//	 * mode.
	//	 */
	//	XOR_PREFIX256(false, true, false, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #XOR} as well as the {@link #MUTATE} mode
	//	 * alongside the {@link #PREFIX256} mode.
	//	 */
	//	MUTATE_XOR_PREFIX256(true, true, false, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode alongside the {@link #PREFIX256}
	//	 * mode.
	//	 */
	//	SALTED_PREFIX256(false, false, true, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #MUTATE}
	//	 * mode alongside the {@link #PREFIX256} mode.
	//	 */
	//	SALTED_MUTATE_PREFIX256(true, false, true, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * alongside the {@link #PREFIX256} mode.
	//	 */
	//	SALTED_XOR_PREFIX256(false, true, true, (short) 256),
	//
	//	/**
	//	 * Combination of the {@link #SALTED} mode as well as the {@link #XOR} mode
	//	 * and the {@link #MUTATE} mode alongside the {@link #PREFIX256} mode.
	//	 */
	//	SALTED_MUTATE_XOR_PREFIX256(true, true, true, (short) 256);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isMutateS;
	private boolean _isXorNext;
	private boolean _isSalted;
	private short _rndPrefixSize;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ChaosMode( boolean isMutateS, boolean isXorNext, boolean isSalted, short aRndPrefixSize ) {
		_isMutateS = isMutateS;
		_isXorNext = isXorNext;
		_isSalted = isSalted;
		_rndPrefixSize = aRndPrefixSize;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isXorNext() {
		return _isXorNext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMutateS() {
		return _isMutateS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSalted() {
		return _isSalted;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short getRndPrefixSize() {
		return _rndPrefixSize;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [isMutateS=" + _isMutateS + ", isXorNext=" + _isXorNext + ", ( isSalted=" + _isSalted + " | hasRndPrefix=" + hasRndPrefix() + " [rndPrefixSize=" + _rndPrefixSize + "]) := isFixedLength=" + isFixedLength() + "]";
	}
}

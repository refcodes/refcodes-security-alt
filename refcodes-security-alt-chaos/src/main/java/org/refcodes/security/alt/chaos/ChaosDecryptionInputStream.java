// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import java.io.IOException;
import java.io.InputStream;

import org.refcodes.security.DecryptionException;
import org.refcodes.security.DecryptionInputStream;

/**
 * A {@link ChaosDecryptionInputStream} wraps an {@link InputStream} and
 * consumes input bytes by applying a {@link ChaosDecrypter} on each byte read
 * from the provided {@link InputStream} before passing back the processed byte
 * to the caller. The input of the {@link ChaosDecryptionInputStream} can be
 * converted back by the according {@link ChaosEncryptionOutputStream}.
 * 
 * In case salting is enabled in the root {@link ChaosKey}, then the
 * {@link ChaosDecryptionInputStream} initially decrypts heading bytes of an
 * {@link InputStream} which then are used as a salt {@link ChaosKey} finally
 * decrypting the decrypted "salted" data (being created using the
 * {@link ChaosEncryptionOutputStream} with salting enabled).
 */
public class ChaosDecryptionInputStream extends DecryptionInputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ChaosDecryptionInputStream} using the given
	 * {@link ChaosKey} for decrypting.
	 *
	 * @param aInputStream The {@link InputStream} to be wrapped.
	 * @param aKey The {@link ChaosKey} to use for decrypting.*
	 * 
	 * @throws IOException Signals that an I/O exception has occurred while
	 *         applying salting.
	 */
	public ChaosDecryptionInputStream( InputStream aInputStream, ChaosKey aKey ) throws IOException {
		super( toInputStrean( aInputStream, aKey ), toChaosDecrypter( aInputStream, aKey ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	private static InputStream toInputStrean( InputStream aInputStream, ChaosKey aKey ) throws IOException {
		aKey = aKey.nextVariableLengthChild();
		return aKey == null ? aInputStream : new ChaosDecryptionInputStream( aInputStream, aKey );
	}

	private static ChaosDecrypter toChaosDecrypter( InputStream aInputStream, ChaosKey aKey ) throws IOException {

		final int theFixedLengthChildDepth = aKey.fixedLengthChildDepth();
		final ChaosDecrypter theDecrypter = new ChaosDecrypter( aKey, theFixedLengthChildDepth );

		try {
			// PREFIX  |-->
			if ( aKey.getOptions().hasRndPrefix() ) {
				final short theSize = aKey.getOptions().getRndPrefixSize();
				final byte[] thePrefix = new byte[theSize];
				final int theRead = aInputStream.read( thePrefix );
				if ( theRead != theSize ) {
					throw new IOException( "Read only <" + theRead + "> bytes of <" + theSize + "> bytes required by the RND prefix." );
				}
				theDecrypter.toDecrypted( thePrefix );
			}
			// PREFIX <--|

			// SALTED |-->
			if ( aKey.getOptions().isSalted() ) {
				final byte[] theEncrypted = new byte[ChaosKey.getEncodedLength()];
				final int theRead = aInputStream.read( theEncrypted, 0, ChaosKey.getEncodedLength() );
				if ( theRead != ChaosKey.getEncodedLength() ) {
					throw new IOException( "Read only <" + theRead + "> bytes of <" + ChaosKey.getEncodedLength() + "> bytes requried for the salt key." );
				}
				final byte[] theEncoding;
				theEncoding = theDecrypter.toDecrypted( theEncrypted );
				final ChaosKey theSaltKey = new ChaosKey( theEncoding );
				return new ChaosDecrypter( theSaltKey, theFixedLengthChildDepth, theDecrypter );
			}
			// SALTED <--|
		}
		catch ( DecryptionException e ) {
			throw new IllegalStateException( "Encountered an illegal sate while decrypting!", e );
		}
		return theDecrypter;
	}
}

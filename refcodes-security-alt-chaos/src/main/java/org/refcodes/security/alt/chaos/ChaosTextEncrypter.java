// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import java.io.UnsupportedEncodingException;

import org.refcodes.codec.BaseBuilder;
import org.refcodes.codec.BaseMetricsConfig;
import org.refcodes.data.Encoding;
import org.refcodes.mixin.Disposable;
import org.refcodes.security.EncryptEncodingException;
import org.refcodes.security.Encrypter;
import org.refcodes.security.EncryptionException;

/**
 * The {@link ChaosTextEncrypter} will use {@link String} as input and output
 * type.
 */
public class ChaosTextEncrypter implements Encrypter<String, String, EncryptionException> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ChaosEncrypter _chaosEncrypter;
	private final BaseBuilder _baseCodec = new BaseBuilder().withBaseMetrics( BaseMetricsConfig.BASE64 );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link ChaosTextEncrypter} using the given
	 * {@link ChaosKey} for encrypting.
	 * 
	 * @param aKey The {@link ChaosKey} to use for encrypting.
	 */
	public ChaosTextEncrypter( ChaosKey aKey ) {
		_chaosEncrypter = new ChaosEncrypter( aKey );
	}

	/**
	 * Instantiates a new {@link ChaosTextEncrypter} using the given
	 * {@link ChaosKey} for encrypting.
	 * 
	 * @param aKey The {@link ChaosKey} to use for encrypting.
	 * @param isVerify When true then the encryption is verified against an
	 *        according live decryption to throw an
	 *        {@link IllegalStateException} in case encryption and decryption
	 *        differ.
	 */
	public ChaosTextEncrypter( ChaosKey aKey, boolean isVerify ) {
		_chaosEncrypter = new ChaosEncrypter( aKey, isVerify );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toEncrypted( String aDecrypted ) throws EncryptionException {
		final byte[] theInput;
		try {
			theInput = aDecrypted.getBytes( Encoding.UTF_8.getCode() );
		}
		catch ( UnsupportedEncodingException e ) {
			throw new EncryptEncodingException( "The required encoding <{0}> is not supported!", Encoding.UTF_8.getCode(), e );
		}
		final byte[] theOutput = new byte[theInput.length];
		toEncrypted( theInput, 0, theInput.length, theOutput, 0 );
		return _baseCodec.toEncodedText( theOutput );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toEncrypted( byte[] aInput, int aInputOffset, int aInputLength, byte[] aOutput, int aOutputOffset ) throws EncryptionException {
		return _chaosEncrypter.toEncrypted( aInput, aInputOffset, aInputLength, aOutput, aOutputOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		final Disposable theChaosEncrypter = _chaosEncrypter;
		if ( theChaosEncrypter != null ) {
			try {
				theChaosEncrypter.dispose();
			}
			catch ( Exception ignore ) { /* ignore */ }
			_chaosEncrypter = null;
		}
	}
}

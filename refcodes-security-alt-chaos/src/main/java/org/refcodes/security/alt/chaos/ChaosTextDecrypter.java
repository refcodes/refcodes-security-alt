// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import java.io.UnsupportedEncodingException;

import org.refcodes.codec.BaseBuilder;
import org.refcodes.codec.BaseMetricsConfig;
import org.refcodes.data.Encoding;
import org.refcodes.mixin.Disposable;
import org.refcodes.security.DecryptEncodingException;
import org.refcodes.security.Decrypter;
import org.refcodes.security.DecryptionException;

/**
 * The {@link ChaosTextDecrypter} will use {@link String} as input and output
 * type.
 */
public class ChaosTextDecrypter implements Decrypter<String, String, DecryptionException> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ChaosDecrypter _chaosDecrypter;
	private final BaseBuilder _baseCodec = new BaseBuilder().withBaseMetrics( BaseMetricsConfig.BASE64 );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link ChaosTextDecrypter} using the given
	 * {@link ChaosKey} for decrypting.
	 * 
	 * @param aKey The {@link ChaosKey} to use for decrypting.
	 */
	public ChaosTextDecrypter( ChaosKey aKey ) {
		_chaosDecrypter = new ChaosDecrypter( aKey );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toDecrypted( String aEncrypted ) throws DecryptionException {
		final byte[] theInput;
		theInput = _baseCodec.toDecodedData( aEncrypted );
		final byte[] theOutput = new byte[theInput.length];
		toDecrypted( theInput, 0, theInput.length, theOutput, 0 );
		try {
			return new String( theOutput, Encoding.UTF_8.getCode() );
		}
		catch ( UnsupportedEncodingException e ) {
			throw new DecryptEncodingException( "The required encoding <{0}> is not supported!", Encoding.UTF_8.getCode(), e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toDecrypted( byte[] aInput, int aInputOffset, int aInputLength, byte[] aOutput, int aOutputOffset ) throws DecryptionException {
		return _chaosDecrypter.toDecrypted( aInput, aInputOffset, aInputLength, aOutput, aOutputOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		final Disposable theChaosDecrypter = _chaosDecrypter;
		if ( theChaosDecrypter != null ) {
			try {
				theChaosDecrypter.dispose();
			}
			catch ( Exception ignore ) { /* ignore */ }
			_chaosDecrypter = null;
		}
	}
}

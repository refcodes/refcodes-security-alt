// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import org.refcodes.mixin.EncodedAccessor;
import org.refcodes.numerical.NumericalUtility;

/**
 * The {@link ChaosOptions} interface provides configuration means for the
 * Chaos-based encryption and may be provided to the constructor of the
 * {@link ChaosKey} e.g. as one of the {@link ChaosMode} enumerations.
 * 
 * Disclaimer: Using the options "stand alone" in a {@link ChaosKey} not in
 * combination with {@link #isSalted()} either in this {@link ChaosKey} or in
 * combination with another {@link ChaosKey} does not (completely) close the
 * "comparison" attack vector (see
 * "https://www.metacodes.pro/funcodes/chaos-based_encryption_revisited")
 */
public interface ChaosOptions extends EncodedAccessor {

	/**
	 * The bit position of the {@link #isMutateS()} option when creating the
	 * encoded representation as of {@link #getEncoded()}.
	 */
	int OPTIONS_BIT_MUTATE_S = 0;

	/**
	 * The bit position of the {@link #isXorNext()} option when creating the
	 * encoded representation as of {@link #getEncoded()}.
	 */
	int OPTIONS_BIT_XOR_NEXT = 1;

	/**
	 * The bit position of the {@link #isSalted()} option when creating the
	 * encoded representation as of {@link #getEncoded()}.
	 */
	int OPTIONS_BIT_SALTED = 2;

	/**
	 * The bit position of the {@link #hasRndPrefix()} option when creating the
	 * encoded representation as of {@link #getEncoded()}.
	 */
	int OPTIONS_BIT_RND_PREFIX = 3;

	/**
	 * The number of bytes required to store the {@link ChaosOptions} flags.
	 */
	int OPTIONS_BYTES = 1;

	/**
	 * The number of bytes required to store the {@link ChaosOptions} RND prefix
	 * size (as of {@link #getRndPrefixSize()}).
	 */
	int PREFIX_SIZE_BYZES = 1;

	/**
	 * The overall encoded length in bytes of the {@link ChaosOptions}.
	 */
	int ENCODED_LENGTH = OPTIONS_BYTES + PREFIX_SIZE_BYZES;

	/**
	 * Determines whether an XOR obfuscation of the next byte value to be
	 * encrypted is to be applied with the previous byte value (before its
	 * encryption or zero 0 in case of the first value being processed). XOR
	 * does not seem to be too effective obfuscating similar encrypted data with
	 * the same {@link ChaosKey}, though for low capability devices it can be
	 * handy to add that little extra obfuscation to make it hard to interpolate
	 * between two similar but different messages.
	 * 
	 * Disclaimer: Using this option "stand alone" in a {@link ChaosKey} not in
	 * combination with {@link #isSalted()} either in this {@link ChaosKey} or
	 * in combination with another {@link ChaosKey} not completely closes the
	 * "comparison" attack vector (see
	 * "https://www.metacodes.pro/funcodes/chaos-based_encryption_revisited")
	 * 
	 * @return True in case XOR obfuscation is to be established.
	 */
	boolean isXorNext();

	/**
	 * Determines whether a MUTATION operation of the {@link ChaosKey}'s S value
	 * is to be applied with the previous byte value (before its encryption or
	 * zero 0 in case of the first value being processed), S then varies by +127
	 * to -128 by each iteration: Nearly equal encrypted datasets will differ
	 * chaotically beginning with the the first occurrence of a different byte.
	 * 
	 * Disclaimer: Using this option "stand alone" in a {@link ChaosKey} not in
	 * combination with {@link #isSalted()} either in this {@link ChaosKey} or
	 * in combination with another {@link ChaosKey} not completely closes the
	 * "comparison" attack vector (see
	 * "https://www.metacodes.pro/funcodes/chaos-based_encryption_revisited")
	 * 
	 * @return True in case ADD obfuscation is to be established.
	 */
	boolean isMutateS();

	/**
	 * Determines whether an additional SALTING operation with an additional
	 * salting (random) {@link ChaosKey} is to be applied: The salting
	 * {@link ChaosKey}'s encoded bytes representation (as of
	 * {@link ChaosKey#getEncoded()}) is prefixed to the actual data which in a
	 * first pass is encoded with the this very salting {@link ChaosKey}. The
	 * salting {@link ChaosKey} encoded bytes representation as well as the
	 * succeeding initially encoded (salted) data then is encoded with the
	 * actual encryption {@link ChaosKey}, ensuring that the prefixed salting
	 * {@link ChaosKey} bytes are encoded. Decryption then is applied in reverse
	 * order. Currently only the {@link ChaosEncryptionOutputStream} and the
	 * {@link ChaosDecryptionInputStream} support salting! The
	 * {@link ChaosEncrypter} and {@link ChaosDecrypter} ignore this option!
	 * 
	 * @return True in case SALTING is to be established.
	 */
	boolean isSalted();

	/**
	 * Determines whether a PREFIX operation of the {@link ChaosKey}'s decrypted
	 * (raw) data with random data when encrypting is to be applied: Prefixing
	 * with random bytes will cause the same (succeeding) decrypted (raw) data
	 * to be encrypted differently even when using the same {@link ChaosKey}
	 * configuration.
	 * 
	 * Disclaimer: Using this option "stand alone" in a {@link ChaosKey} not in
	 * combination with {@link #isSalted()} either in this {@link ChaosKey} or
	 * in combination with another {@link ChaosKey} not close by any means the
	 * "comparison" attack vector (see
	 * "https://www.metacodes.pro/funcodes/chaos-based_encryption_revisited")
	 * 
	 * @return True in case PREFIX is to be established.
	 */
	default boolean hasRndPrefix() {
		return getRndPrefixSize() != 0;
	}

	/**
	 * Sets the number of random bytes to be prefixed upon a PREFIX operation of
	 * decrypted (raw) data when encrypting with the accordingly configured
	 * {@link ChaosKey}: Prefixing with random bytes will cause the same
	 * (succeeding) decrypted (raw) data to be encrypted differently even when
	 * using the same {@link ChaosKey} configuration. Valid values range from
	 * {1..256} representable by one byte as of {0x00..0xFF}.
	 * 
	 * Disclaimer: Using this option "stand alone" in a {@link ChaosKey} not in
	 * combination with {@link #isSalted()} either in this {@link ChaosKey} or
	 * in combination with another {@link ChaosKey} not close by any means the
	 * "comparison" attack vector (see
	 * "https://www.metacodes.pro/funcodes/chaos-based_encryption_revisited")
	 * 
	 * @return The number of bytes for establishing the PREFIX functionality, 0
	 *         if no prefixing is to be applied at all.
	 */
	short getRndPrefixSize();

	/**
	 * Determines whether applying an according configured {@link ChaosKey}
	 * associates one element of the encrypted data with exactly one element of
	 * the decrypted (raw) data: Any {@link ChaosOptions} option affecting the
	 * length of the encrypted data compared to the length of the decrypted
	 * (raw) data is considered in this context not(!) to be a fixed length
	 * {@link ChaosKey}. E.g. salting (being declared with the
	 * {@link ChaosOptions#isSalted()} option) is not(!) considered to be of
	 * fixed length as salting decrypted (raw) data during encryption will
	 * result in encrypted data being longer than the decrypted (raw) data (by
	 * the length of the salting key). Non fixed length options are only
	 * supported by streams such as the {@link ChaosEncryptionOutputStream} or
	 * the {@link ChaosDecryptionInputStream}!
	 * 
	 * @return True when these {@link ChaosOptions} represent an injective
	 *         configuration.
	 */
	default boolean isFixedLength() {
		return !isSalted() & !hasRndPrefix();
	}

	/**
	 * Returns the options in their primary encoding format. Returns the bytes
	 * in a defined order representing the {@link ChaosOptions}. The values use
	 * a big endian representation. The byte array being returned is of the size
	 * as returned by {@link #getEncodedLength()}. The meaning of the bits in
	 * the encoded byte are defined as in {@link #OPTIONS_BIT_MUTATE_S},
	 * {@link #OPTIONS_BIT_XOR_NEXT} and {@link #OPTIONS_BIT_SALTED}.
	 * 
	 * @return The encoded options being the according byte(s) representing the
	 *         {@link ChaosOptions}.
	 */
	@Override
	default byte[] getEncoded() {
		final byte[] theBytes = { 0, 0 };
		theBytes[0] = NumericalUtility.setBitAt( theBytes[0], OPTIONS_BIT_MUTATE_S, isMutateS() );
		theBytes[0] = NumericalUtility.setBitAt( theBytes[0], OPTIONS_BIT_XOR_NEXT, isXorNext() );
		theBytes[0] = NumericalUtility.setBitAt( theBytes[0], OPTIONS_BIT_SALTED, isSalted() );
		theBytes[0] = NumericalUtility.setBitAt( theBytes[0], OPTIONS_BIT_RND_PREFIX, hasRndPrefix() );
		theBytes[1] = hasRndPrefix() ? (byte) ( getRndPrefixSize() - 1 ) : (byte) 0;
		return theBytes;
	}

	/**
	 * Returns the length of this {@link ChaosOptions} when represented as bytes
	 * (as of {@link #getEncoded()}).
	 * 
	 * @return The number of bytes required to represent a {@link ChaosOptions}
	 *         settings.
	 */
	static int getEncodedLength() {
		return ENCODED_LENGTH;
	}

	/**
	 * Returns true if all the passed object is of type {@link ChaosOptions} and
	 * all the attributes from the {@link ChaosOptions} are equal to this
	 * instance's attributes (as of {@link #isMutateS()}, {@link #isXorNext()}
	 * and {@link #isSalted()}).
	 *
	 * @param aObj the object to compare with for equality.
	 * 
	 * @return true, if equal
	 */
	@Override
	boolean equals( Object aObj );
}

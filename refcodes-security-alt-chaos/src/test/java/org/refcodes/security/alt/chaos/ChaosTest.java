// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.ShortBufferException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.DecryptionException;
import org.refcodes.security.EncryptionException;

public class ChaosTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testChaos() throws EncryptionException, DecryptionException {
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( !eMode.isFixedLength() ) {
				continue;
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Chaos mode := " + eMode );
			}
			final String theMessage = "Hello world, chaos is here to stay!";
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Message        := " + theMessage );
			}
			final byte[] theMessageHex = theMessage.getBytes();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Hex Message    := " + Arrays.toString( theMessageHex ) );
			}
			final ChaosKey theChaosKey = new ChaosKey( 0.67, 3.61, 12536, eMode );
			final ChaosEncrypter theEncrypter = new ChaosEncrypter( theChaosKey );
			final byte[] theEncrypted = theEncrypter.toEncrypted( theMessageHex );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted Hex  := " + Arrays.toString( theEncrypted ) );
			}
			final ChaosDecrypter theDecrypter = new ChaosDecrypter( theChaosKey );
			final byte[] theDecrypted = theDecrypter.toDecrypted( theEncrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Hex  := " + Arrays.toString( theDecrypted ) );
			}
			final String theText = new String( theDecrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Text := " + theText );
			}
			assertEquals( theMessage, theText );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
			}
		}
	}

	@Test
	public void testWithChaosChild() throws EncryptionException, DecryptionException {
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( !eMode.isFixedLength() ) {
				continue;
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Chaos mode := " + eMode );
			}
			final String theMessage = "Hello world, chaos is here to stay!";
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Message        := " + theMessage );
			}
			final byte[] theMessageHex = theMessage.getBytes();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Hex Message    := " + Arrays.toString( theMessageHex ) );
			}
			final ChaosKey theChaosKey = new ChaosKey( 0.67, 3.61, 12536, eMode, ChaosKey.createRndKey() );
			final ChaosEncrypter theEncrypter = new ChaosEncrypter( theChaosKey, true );
			final byte[] theEncrypted = theEncrypter.toEncrypted( theMessageHex );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted Hex  := " + Arrays.toString( theEncrypted ) );
			}
			final ChaosDecrypter theDecrypter = new ChaosDecrypter( theChaosKey );
			final byte[] theDecrypted = theDecrypter.toDecrypted( theEncrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Hex  := " + Arrays.toString( theDecrypted ) );
			}
			final String theText = new String( theDecrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Text := " + theText );
			}
			assertEquals( theMessage, theText );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
			}
		}
	}

	@Test
	public void testChaosText() throws EncryptionException, DecryptionException {
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( !eMode.isFixedLength() ) {
				continue;
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Chaos mode := " + eMode );
			}
			final String theMessage = "Hello world, chaos is here to stay!";
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Message   := " + theMessage );
			}
			final ChaosKey theChaosKey = new ChaosKey( 0.67, 3.61, 12536, eMode );
			final ChaosTextEncrypter theEncrypter = new ChaosTextEncrypter( theChaosKey );
			final String theEncrypted = theEncrypter.toEncrypted( theMessage );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted := " + theEncrypted );
			}
			final ChaosTextDecrypter theDecrypter = new ChaosTextDecrypter( theChaosKey );
			final String theDecrypted = theDecrypter.toDecrypted( theEncrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted := " + theDecrypted );
			}
			assertEquals( theMessage, theDecrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
			}
		}
	}

	@Test
	public void testChaosCompare() throws EncryptionException, DecryptionException, ShortBufferException {
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( !eMode.isFixedLength() ) {
				continue;
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Chaos mode := " + eMode );
			}
			final String theMessage = "Hello world, chaos is here to stay!";
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Message          := " + theMessage );
			}
			final byte[] theMessageHex = theMessage.getBytes();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Hex Message      := " + Arrays.toString( theMessageHex ) );
			}
			final ChaosKey theChaosKey = new ChaosKey( 0.67, 3.61, 12536, eMode );
			final ChaosEncrypter theEncrypter1 = new ChaosEncrypter( theChaosKey );
			final byte[] theEncrypted1 = theEncrypter1.toEncrypted( theMessageHex );
			final ChaosEncrypter theEncrypter2 = new ChaosEncrypter( theChaosKey );
			final byte[] theEncrypted2 = new byte[theMessageHex.length];
			theEncrypter2.toEncrypted( theMessageHex, 0, theMessageHex.length, theEncrypted2, 0 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted Hex 1  := " + Arrays.toString( theEncrypted1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted Hex 2  := " + Arrays.toString( theEncrypted2 ) );
			}
			final ChaosDecrypter theDecrypter1 = new ChaosDecrypter( theChaosKey );
			final byte[] theDecrypted1 = theDecrypter1.toDecrypted( theEncrypted1 );
			final ChaosDecrypter theDecrypter2 = new ChaosDecrypter( theChaosKey );
			final byte[] theDecrypted2 = new byte[theMessageHex.length];
			theDecrypter2.toDecrypted( theEncrypted2, 0, theMessageHex.length, theDecrypted2, 0 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Hex 1  := " + Arrays.toString( theDecrypted1 ) );
			}
			final String theText1 = new String( theDecrypted1 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Hex 2  := " + Arrays.toString( theDecrypted2 ) );
			}
			final String theText2 = new String( theDecrypted2 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Text 1 := " + theText1 );
			}
			assertEquals( theMessage, theText1 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted Text 2 := " + theText2 );
			}
			assertEquals( theMessage, theText2 );
		}
	}

	@Disabled
	@Test
	public void testChaosDistribution() throws EncryptionException {
		final ChaosKey theChaosKey = new ChaosKey( 0.67, 3.61, 12536 );
		final ChaosEncrypter theEncrypter = new ChaosEncrypter( theChaosKey );
		final Map<Byte, Integer> theMap = new HashMap<>();
		final byte[] theDecrypted = new byte[] { 1 };
		Byte eKey;
		Integer eValue;
		for ( int i = 0; i < 100000; i++ ) {
			eKey = theEncrypter.toEncrypted( theDecrypted )[0];
			eValue = theMap.get( eKey );
			if ( eValue == null ) {
				eValue = 1;
			}
			eValue++;
			theMap.put( eKey, eValue );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			int theMin = -1;
			int theMax = 0;
			int theTotal = 0;
			for ( Byte eEnc : theMap.keySet() ) {
				eValue = theMap.get( eEnc );
				theTotal += eValue;
				if ( theMin == -1 || eValue < theMin ) {
					theMin = eValue;
				}
				if ( eValue > theMax ) {
					theMax = eValue;
				}
				System.out.println( eEnc + " := " + eValue );
				System.out.println( eEnc + " := " + eValue );
			}
			System.out.println( "   Min  := " + theMin );
			System.out.println( "   Max  := " + theMax );
			System.out.println( "Average := " + ( theTotal / theMap.size() ) );
		}
	}
}

// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import org.refcodes.exception.BugException;

public class AbstractStreamTest {
	static final int DEFAULT_BUFFER_SIZE = 8192;

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected static long transferTo( InputStream aInputStream, OutputStream aOutputStream, InputStream aDiffStream ) throws IOException {
		Objects.requireNonNull( aOutputStream, "out" );
		long transferred = 0;
		final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		int read;
		byte buf;
		while ( ( read = aInputStream.read( buffer, 0, DEFAULT_BUFFER_SIZE ) ) >= 0 ) {
			aOutputStream.write( buffer, 0, read );
			for ( int i = 0; i < read; i++ ) {
				buf = (byte) ( 0xff & ( (int) buffer[i] ) );
				if ( aDiffStream.read() != buf ) {
					throw new BugException( "Difference at position <" + ( transferred + i ) + ">!" );
				}
			}
			transferred += read;
		}
		return transferred;
	}
}
// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.MemoryUnit;
import org.refcodes.data.Text;
import org.refcodes.exception.Trap;
import org.refcodes.io.FileUtility;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.EncryptionException;
import org.refcodes.textual.RandomTextGenerartor;

public class ChaosEncryptionDecryptionStreamTest extends AbstractStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@SuppressWarnings("resource")
	public void testChaosEncryptionDecryptionStream() throws IOException {
		final byte[] theSenderPayload = Text.ARECIBO_MESSAGE.getText().getBytes();
		final ChaosKey theKey = ChaosKey.createRndKey();
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final ChaosEncryptionOutputStream theChaosOutputStream = new ChaosEncryptionOutputStream( theOutputStream, theKey );
		theChaosOutputStream.write( theSenderPayload );
		theChaosOutputStream.flush();
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		final ChaosDecryptionInputStream theChaosInputStream = new ChaosDecryptionInputStream( theInputStream, theKey );
		final byte[] theReceiverPayload = new byte[theSenderPayload.length];
		theChaosInputStream.read( theReceiverPayload, 0, theReceiverPayload.length );
		final String theMessage = new String( theReceiverPayload );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMessage );
		}
		assertArrayEquals( theSenderPayload, theReceiverPayload );
	}

	@Test
	public void testChaosFileEncryptionDecryption1() throws IOException {
		ChaosKey eKey;
		String eSecret;
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE, MemoryUnit.MEGABYTE };
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( eMode.hasRndPrefix() && eMode.getRndPrefixSize() != 16 ) {
				continue;
			}
			for ( MemoryUnit eMemUnit : theMemUnits ) {
				for ( int i = 1; i < 4; i++ ) {
					eSecret = RandomTextGenerartor.asString( RND.nextInt( 16 ) + 8 );
					eKey = new ChaosKey( eSecret, eMode );
					long eSize = eMemUnit.toBytes( i ).longValue();
					if ( eMemUnit != MemoryUnit.BYTE ) {
						eSize += RND.nextInt( 32 ); // Make the length vary a little (test the padding)
					}
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Testing file size <" + eSize + "> for key <" + eKey + "> with secret <" + eSecret + "> ..." );
					}
					final File theBigFile = FileUtility.createRandomTempFile( eSize );
					final File theEncodedFile = FileUtility.createTempFile();
					final File theDecodedFile = FileUtility.createTempFile();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Chaos mode := " + eMode );
						System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
						System.out.println( "Encrypted file := " + theEncodedFile.getAbsolutePath() );
						System.out.println( "Decrypted file := " + theDecodedFile.getAbsolutePath() );
					}
					try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); ChaosEncryptionOutputStream theEncoderOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eKey, true ) ) {
						theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended! 
					}
					catch ( Exception e ) {
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						e.printStackTrace();
						fail( "Data verification failed!" );
					}
					ChaosDecryptionInputStream theDecoderInputRef = null;
					try ( ChaosDecryptionInputStream theDecoderInputStream = new ChaosDecryptionInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eKey ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
						theDecoderInputRef = theDecoderInputStream;
						final BufferedInputStream theDiffStream = new BufferedInputStream( new FileInputStream( theBigFile ) );
						transferTo( theDecoderInputStream, theDecodedOutputStream, theDiffStream );
					}
					catch ( Exception e ) {
						System.out.println( theDecoderInputRef.toString() );
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						fail( "The files are not(!) equal!" );
					}
					if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
						System.out.println( eKey.toString() );
						fail( "The files are not(!) equal!" );
					}
					delete( theBigFile, "BIG" );
					delete( theEncodedFile, "ENCODED" );
					delete( theDecodedFile, "DECODED" );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println();
					}
				}
			}
		}
	}

	@Test
	public void testChaosFileEncryptionDecryption2() throws IOException {
		ChaosKey eKey;
		String eSecret;
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE, MemoryUnit.MEGABYTE };
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( eMode.hasRndPrefix() && eMode.getRndPrefixSize() != 16 ) {
				continue;
			}
			for ( MemoryUnit eMemUnit : theMemUnits ) {
				for ( int i = 1; i < 4; i++ ) {
					eSecret = RandomTextGenerartor.asString( RND.nextInt( 16 ) + 8 );
					eKey = new ChaosKey( eSecret, eMode, ChaosKey.createRndKey() );
					long eSize = eMemUnit.toBytes( i ).longValue();
					if ( eMemUnit != MemoryUnit.BYTE ) {
						eSize += RND.nextInt( 32 ); // Make the length vary a little (test the padding)
					}
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Testing file size <" + eSize + "> for key <" + eKey + "> with secret <" + eSecret + "> ..." );
					}
					final File theBigFile = FileUtility.createRandomTempFile( eSize );
					final File theEncodedFile = FileUtility.createTempFile();
					final File theDecodedFile = FileUtility.createTempFile();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Chaos mode := " + eMode );
						System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
						System.out.println( "Encrypted file := " + theEncodedFile.getAbsolutePath() );
						System.out.println( "Decrypted file := " + theDecodedFile.getAbsolutePath() );
					}
					try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); ChaosEncryptionOutputStream theEncoderOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eKey, true ) ) {
						theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended! 
					}
					catch ( Exception e ) {
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						fail( "Data verification failed!" );
					}
					ChaosDecryptionInputStream theDecoderInputRef = null;
					try ( ChaosDecryptionInputStream theDecoderInputStream = new ChaosDecryptionInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eKey ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
						theDecoderInputRef = theDecoderInputStream;
						final BufferedInputStream theDiffStream = new BufferedInputStream( new FileInputStream( theBigFile ) );
						transferTo( theDecoderInputStream, theDecodedOutputStream, theDiffStream );
					}
					catch ( Exception e ) {
						System.out.println( theDecoderInputRef.toString() );
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						fail( "The files are not(!) equal!" );
					}
					if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
						System.out.println( eKey.toString() );
						fail( "The files are not(!) equal!" );
					}
					delete( theBigFile, "BIG" );
					delete( theEncodedFile, "ENCODED" );
					delete( theDecodedFile, "DECODED" );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println();
					}
				}
			}
		}
	}

	@Test
	public void testChaosFileEncryptionDecryption3() throws IOException {
		ChaosKey eKey;
		String eSecret;
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE, MemoryUnit.MEGABYTE };
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( eMode.hasRndPrefix() && eMode.getRndPrefixSize() != 16 ) {
				continue;
			}
			for ( MemoryUnit eMemUnit : theMemUnits ) {
				for ( int i = 1; i < 4; i++ ) {
					eSecret = RandomTextGenerartor.asString( RND.nextInt( 16 ) + 8 );
					eKey = new ChaosKey( eSecret, eMode, ChaosKey.createRndKey( ChaosKey.createRndKey( eMode ) ) );
					long eSize = eMemUnit.toBytes( i ).longValue();
					if ( eMemUnit != MemoryUnit.BYTE ) {
						eSize += RND.nextInt( 32 ); // Make the length vary a little (test the padding)
					}
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Testing file size <" + eSize + "> for key <" + eKey + "> with secret <" + eSecret + "> ..." );
					}
					final File theBigFile = FileUtility.createRandomTempFile( eSize );
					final File theEncodedFile = FileUtility.createTempFile();
					final File theDecodedFile = FileUtility.createTempFile();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Chaos mode := " + eMode );
						System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
						System.out.println( "Encrypted file := " + theEncodedFile.getAbsolutePath() );
						System.out.println( "Decrypted file := " + theDecodedFile.getAbsolutePath() );
					}
					try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); ChaosEncryptionOutputStream theEncoderOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eKey, true ) ) {
						theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended!
					}
					catch ( Exception e ) {
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						e.printStackTrace();
						fail( "Data verification failed!" );
					}
					ChaosDecryptionInputStream theDecoderInputRef = null;
					try ( ChaosDecryptionInputStream theDecoderInputStream = new ChaosDecryptionInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eKey ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
						theDecoderInputRef = theDecoderInputStream;
						final BufferedInputStream theDiffStream = new BufferedInputStream( new FileInputStream( theBigFile ) );
						transferTo( theDecoderInputStream, theDecodedOutputStream, theDiffStream );
					}
					catch ( Exception e ) {
						System.out.println( theDecoderInputRef.toString() );
						System.out.println( eKey.toString() );
						System.out.println( Trap.asMessage( e ) );
						fail( "The files are not(!) equal!" );
					}
					if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
						System.out.println( eKey.toString() );
						fail( "The files are not(!) equal!" );
					}
					delete( theBigFile, "BIG" );
					delete( theEncodedFile, "ENCODED" );
					delete( theDecodedFile, "DECODED" );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println();
					}
				}
			}
		}
	}

	@Disabled
	@Test
	public void testEdgeCaseNarrowNegative() throws IOException {
		final double x = 0.090133521282176;
		final double a = 3.70256295404982;
		BigDecimal sPass = BigDecimal.valueOf( -8292271207831449L );
		BigDecimal sFail = BigDecimal.valueOf( -10253427413588223L );
		final BigDecimal TWO = BigDecimal.valueOf( 2 );
		while ( true ) {
			final BigDecimal s = sPass.add( sFail ).divide( TWO, RoundingMode.HALF_DOWN );
			final boolean isOk = isValid( x, a, s.longValue(), ChaosMode.NONE );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ( isOk ? "PASS: " : "FAIL: " ) + "x=" + x + ", a=" + a + ", s=" + s + ", sPass=" + sPass + ", sFail=" + sFail );
			}
			if ( isOk ) {
				sPass = s;
			}
			else {
				sFail = s;
			}
		}
	}

	@Disabled
	@Test
	public void testEdgeCaseDetermineNegative() throws IOException {
		final double x = 0.090133521282176;
		final double a = 3.70256295404982;
		// long LAST_S = -9730772296405368L;
		final long LAST_S = -9730772296405368L;
		for ( long s = LAST_S; s < -15; s++ ) {
			out: {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Probing ... x=" + x + ", a=" + a + ", s=" + s );
				}
				for ( int i = 0; i < 10000; i++ ) {
					if ( !isValid( x, a, s, ChaosMode.NONE ) ) {
						break out;
					}
				}
				System.out.println( "Found <" + s + ">!" );
				return;
			}
		}
	}

	@Disabled
	@Test
	public void testEdgeCaseNarrowPositive() throws IOException {
		final double x = 0.090133521282176;
		final double a = 3.70256295404982;
		BigDecimal sPass = BigDecimal.valueOf( 9469010147431344L );
		BigDecimal sFail = BigDecimal.valueOf( 10706240291603714L );
		final BigDecimal TWO = BigDecimal.valueOf( 2 );
		while ( true ) {
			final BigDecimal s = sPass.add( sFail ).divide( TWO, RoundingMode.HALF_DOWN );
			final boolean isOk = isValid( x, a, s.longValue(), ChaosMode.NONE );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ( isOk ? "PASS: " : "FAIL: " ) + "x=" + x + ", a=" + a + ", s=" + s + ", sPass=" + sPass + ", sFail=" + sFail );
			}
			if ( isOk ) {
				sPass = s;
			}
			else {
				sFail = s;
			}
		}
	}

	@Disabled
	@Test
	public void testEdgeCaseDeterminePositive() throws IOException {
		final double x = 0.090133521282176;
		final double a = 3.70256295404982;
		// long LAST_S = 9730772296405068L;
		final long LAST_S = 9730772296405025L;
		for ( long s = LAST_S; s > 15; s-- ) {
			out: {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Probing ... x=" + x + ", a=" + a + ", s=" + s );
				}
				for ( int i = 0; i < 10000; i++ ) {
					if ( !isValid( x, a, s, ChaosMode.NONE ) ) {
						break out;
					}
				}
				System.out.println( "Found <" + s + ">!" );
				return;
			}
		}
	}

	@Test
	public void testEdgeCaseRndPrefix() throws IOException {
		ChaosKey eKey;
		String eSecret;
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE, MemoryUnit.MEGABYTE };
		final ChaosOptions theMode = new ChaosOptionsImpl( true, false, false, (short) 256 );
		for ( MemoryUnit eMemUnit : theMemUnits ) {
			for ( int i = 1; i < 4; i++ ) {
				eSecret = RandomTextGenerartor.asString( RND.nextInt( 16 ) + 8 );
				eKey = new ChaosKey( eSecret, theMode );
				long eSize = eMemUnit.toBytes( i ).longValue();
				if ( eMemUnit != MemoryUnit.BYTE ) {
					eSize += RND.nextInt( 32 ); // Make the length vary a little (test the padding)
				}
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Testing file size <" + eSize + "> for key <" + eKey + "> with secret <" + eSecret + "> ..." );
				}
				final File theBigFile = FileUtility.createRandomTempFile( eSize );
				final File theEncodedFile = FileUtility.createTempFile();
				final File theDecodedFile = FileUtility.createTempFile();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Chaos mode := " + theMode );
					System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
					System.out.println( "Encrypted file := " + theEncodedFile.getAbsolutePath() );
					System.out.println( "Decrypted file := " + theDecodedFile.getAbsolutePath() );
				}
				try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); ChaosEncryptionOutputStream theEncoderOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eKey, true ) ) {
					theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended! 
				}
				catch ( Exception e ) {
					System.out.println( eKey.toString() );
					System.out.println( Trap.asMessage( e ) );
					e.printStackTrace();
					fail( "Data verification failed!" );
				}
				ChaosDecryptionInputStream theDecoderInputRef = null;
				try ( ChaosDecryptionInputStream theDecoderInputStream = new ChaosDecryptionInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eKey ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
					theDecoderInputRef = theDecoderInputStream;
					final BufferedInputStream theDiffStream = new BufferedInputStream( new FileInputStream( theBigFile ) );
					transferTo( theDecoderInputStream, theDecodedOutputStream, theDiffStream );
				}
				catch ( Exception e ) {
					System.out.println( theDecoderInputRef.toString() );
					System.out.println( eKey.toString() );
					System.out.println( Trap.asMessage( e ) );
					fail( "The files are not(!) equal!" );
				}
				if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
					System.out.println( eKey.toString() );
					fail( "The files are not(!) equal!" );
				}
				delete( theBigFile, "BIG" );
				delete( theEncodedFile, "ENCODED" );
				delete( theDecodedFile, "DECODED" );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println();
				}
			}
		}
	}

	@Test
	@Disabled
	public void testEndlessRandomKeys() throws IOException {
		double x;
		double a;
		long s;
		final Random rnd = new Random();
		while ( true ) {
			for ( ChaosMode eMode : ChaosMode.values() ) {
				x = ChaosKey.toX0( rnd.nextLong() );
				a = ChaosKey.toA( rnd.nextLong() );
				s = ChaosKey.toS( rnd.nextLong() );
				isValid( x, a, s, eMode );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void delete( File aFile, String aString ) {
		if ( aFile.delete() ) {
			return;
		}
		try {
			Files.deleteIfExists( aFile.toPath() );
			return;
		}
		catch ( IOException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Cannot delete " + aString + " file <" + aFile.getAbsolutePath() + ">!!! :-(" );
				System.out.println( Trap.asMessage( e ) );
			}
		}
	}

	private static boolean isValid( double x, double a, long s, ChaosOptions aMetrics ) throws IOException {
		final ChaosKey eKey;
		eKey = new ChaosKey( x, a, s, aMetrics );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( eKey.toString() );
		}
		final long eSize = MemoryUnit.MEGABYTE.toBytes( 32 ).longValue();
		final File theBigFile = FileUtility.createRandomTempFile( eSize );
		final File theEncodedFile = FileUtility.createTempFile();
		try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); ChaosEncryptionOutputStream theEncoderOutputStream = new ChaosEncryptionOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eKey, true ) ) {
			theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended!
			return true;
		}
		catch ( IOException e ) {
			if ( e.getCause() instanceof EncryptionException ) {
				return false;
			}
			throw e;
		}
		finally {
			theBigFile.delete();
			theEncodedFile.delete();
		}
	}
}

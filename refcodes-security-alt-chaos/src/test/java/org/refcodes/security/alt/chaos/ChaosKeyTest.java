// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.alt.chaos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

public class ChaosKeyTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testFixedLengthChaosKey1() {
		final ChaosKey theKey1 = ChaosKey.createRndKey( ChaosMode.SALTED );
		final ChaosKey eKey = theKey1.nextVariableLengthChild();
		final int eDepth = theKey1.fixedLengthChildDepth();
		assertEquals( null, eKey );
		assertEquals( 0, eDepth );
	}

	@Test
	public void testFixedLengthChaosKey2() {
		final ChaosKey theKey1 = ChaosKey.createRndKey( ChaosMode.NONE );
		final ChaosKey eKey = theKey1.nextVariableLengthChild();
		final int eDepth = theKey1.fixedLengthChildDepth();
		assertEquals( null, eKey );
		assertEquals( 0, eDepth );
	}

	@Test
	public void testFixedLengthChaosKey3() {
		final ChaosKey theKey6 = ChaosKey.createRndKey( ChaosMode.XOR );
		final ChaosKey theKey5 = ChaosKey.createRndKey( ChaosMode.NONE, theKey6 );
		final ChaosKey theKey4 = ChaosKey.createRndKey( new ChaosOptionsImpl( false, false, false, (short) 1 ), theKey5 );
		final ChaosKey theKey3 = ChaosKey.createRndKey( ChaosMode.XOR, theKey4 );
		final ChaosKey theKey2 = ChaosKey.createRndKey( ChaosMode.NONE, theKey3 );
		final ChaosKey theKey1 = ChaosKey.createRndKey( ChaosMode.SALTED, theKey2 );
		int eDepth = theKey1.fixedLengthChildDepth();
		ChaosKey eKey = theKey1.nextVariableLengthChild();
		assertEquals( theKey4, eKey );
		assertEquals( 2, eDepth );
		eDepth = eKey.fixedLengthChildDepth();
		eKey = eKey.nextVariableLengthChild();
		assertEquals( null, eKey );
		assertEquals( 2, eDepth );
	}

	@Test
	public void testFixedLengthChaosKey4() {
		final ChaosKey theKey7 = ChaosKey.createRndKey( new ChaosOptionsImpl( false, false, false, (short) 256 ) );
		final ChaosKey theKey6 = ChaosKey.createRndKey( ChaosMode.XOR, theKey7 );
		final ChaosKey theKey5 = ChaosKey.createRndKey( ChaosMode.NONE, theKey6 );
		final ChaosKey theKey4 = ChaosKey.createRndKey( new ChaosOptionsImpl( false, false, false, (short) 1 ), theKey5 );
		final ChaosKey theKey3 = ChaosKey.createRndKey( ChaosMode.XOR, theKey4 );
		final ChaosKey theKey2 = ChaosKey.createRndKey( ChaosMode.NONE, theKey3 );
		final ChaosKey theKey1 = ChaosKey.createRndKey( ChaosMode.SALTED, theKey2 );
		int eDepth = theKey1.fixedLengthChildDepth();
		ChaosKey eKey = theKey1.nextVariableLengthChild();
		assertEquals( theKey4, eKey );
		assertEquals( 2, eDepth );
		eDepth = eKey.fixedLengthChildDepth();
		eKey = eKey.nextVariableLengthChild();
		assertEquals( theKey7, eKey );
		assertEquals( 2, eDepth );
	}

	@Test
	public void testFixedLengthChaosKey5() {
		final ChaosKey theKey7 = ChaosKey.createRndKey( new ChaosOptionsImpl( false, false, false, (short) 256 ) );
		final ChaosKey theKey6 = ChaosKey.createRndKey( ChaosMode.XOR, theKey7 );
		final ChaosKey theKey5 = ChaosKey.createRndKey( ChaosMode.NONE, theKey6 );
		final ChaosKey theKey4 = ChaosKey.createRndKey( new ChaosOptionsImpl( false, false, false, (short) 1 ), theKey5 );
		final ChaosKey theKey3 = ChaosKey.createRndKey( ChaosMode.MUTATE_XOR, theKey4 );
		final ChaosKey theKey2 = ChaosKey.createRndKey( ChaosMode.NONE, theKey3 );
		final ChaosKey theKey1 = ChaosKey.createRndKey( ChaosMode.MUTATE, theKey2 );
		int eDepth = theKey1.fixedLengthChildDepth();
		ChaosKey eKey = theKey1.nextVariableLengthChild();
		assertEquals( theKey4, eKey );
		assertEquals( 2, eDepth );
		eDepth = eKey.fixedLengthChildDepth();
		eKey = eKey.nextVariableLengthChild();
		assertEquals( theKey7, eKey );
		assertEquals( 2, eDepth );
	}

	@Test
	public void testFixedLengthChaosKey6() {
		final ChaosKey theKey = new ChaosKey( "Secret123!", ChaosMode.SALTED, ChaosKey.createRndKey( ChaosKey.createRndKey( ChaosMode.SALTED ) ) );
		int eDepth = theKey.fixedLengthChildDepth();
		ChaosKey eKey = theKey.nextVariableLengthChild();
		assertEquals( ChaosMode.SALTED, eKey.getOptions() );
		assertNotEquals( theKey, eKey );
		assertEquals( 1, eDepth );
		eDepth = eKey.fixedLengthChildDepth();
		eKey = eKey.nextVariableLengthChild();
		assertNull( eKey );
		assertEquals( 0, eDepth );
	}

	@Test
	public void testCertificate1() {
		ChaosKey eChaosKey;
		for ( int i = 1; i < 200; i++ ) {
			eChaosKey = ChaosKey.createRndKeyChain( i, ChaosMode.SALTED, ChaosMode.NONE, ChaosMode.MUTATE_XOR );
			final String theCert = eChaosKey.toCertificate();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Chaos key: " + eChaosKey.toString() );
				System.out.println( theCert );
			}
			final ChaosKey theCertKey = ChaosKey.createFromCertificate( theCert );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Cert key: " + theCertKey.toString() );
				System.out.println( "--------------------------------------------------------------------------------" );
				System.out.println();
			}
			assertEquals( theCertKey, eChaosKey );
		}
	}

	@Test
	public void testSecuredCertificate1() {
		String ePassword;
		ChaosKey eChaosKey;
		for ( ChaosMode eMode : ChaosMode.values() ) {
			if ( !eMode.isFixedLength() ) {
				continue;
			}
			for ( int i = 1; i < 200; i++ ) {
				ePassword = RandomTextGenerartor.asString( RND.nextInt( 32 ) + 1, RandomTextMode.ALPHANUMERIC );
				eChaosKey = ChaosKey.createRndKeyChain( i, ChaosMode.SALTED, ChaosMode.NONE, ChaosMode.MUTATE_XOR );
				final String theCert = eChaosKey.toCertificate( ePassword, eMode );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Chaos key: " + eChaosKey.toString() );
					System.out.println( theCert );
				}
				final ChaosKey theCertKey = ChaosKey.createFromCertificate( theCert, ePassword );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Cert key: " + theCertKey.toString() );
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println();
				}
				assertEquals( theCertKey, eChaosKey );
			}
		}
	}

	@Test
	public void testSecuredCertificate2() {
		final ChaosKey theChaosKey = ChaosKey.createRndKeyChain( 10, ChaosMode.SALTED, ChaosMode.NONE, ChaosMode.MUTATE_XOR );
		final String theCert = theChaosKey.toCertificate( "Hallo Welt!", ChaosMode.SALTED_MUTATE_XOR );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Chaos key: " + theChaosKey.toString() );
			System.out.println( theCert );
		}
		try {
			final ChaosKey theCertKey = ChaosKey.createFromCertificate( theCert, "Hallo Welt" );
			assertNotEquals( theCertKey, theChaosKey );
		}
		catch ( Exception expected ) {}
	}

	@Test
	public void testChaosKeyChainBytes1() {
		final byte[] theExpected = new byte[] { 63, -32, 0, 0, 0, 0, 0, 0, 64, 14, 71, -82, 20, 122, -31, 72, 0, 0, 0, 0, 0, 0, 16, 0, 0, 63, -16, 0, 0, 0, 0, 0, 0, 64, 16, 0, 0, 0, 0, 0, 0, 31, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 12, -113, 92, 40, -11, -62, -113, -32, 0, 0, 0, 0, 0, 0, 0, 0 };
		final ChaosKey theKey3 = new ChaosKey( ChaosKey.X_MIN, ChaosKey.A_MIN, ChaosKey.S_MIN );
		final ChaosKey theKey2 = new ChaosKey( ChaosKey.X_MAX, ChaosKey.A_MAX, ChaosKey.S_MAX, theKey3 );
		final ChaosKey theKey1 = new ChaosKey( ( ChaosKey.X_MIN + ChaosKey.X_MAX ) / 2, ( ChaosKey.A_MIN + ChaosKey.A_MAX ) / 2, ChaosKey.S_POSITIVE_MIN, theKey2 );
		final byte[] theKeyChain = theKey1.toEncodedChain();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Key1 = " + Arrays.toString( theKey1.getEncoded() ) );
			System.out.println( "Key2 = " + Arrays.toString( theKey2.getEncoded() ) );
			System.out.println( "Key3 = " + Arrays.toString( theKey3.getEncoded() ) );
			System.out.println( "Key chain = " + Arrays.toString( theKeyChain ) );
		}
		assertArrayEquals( theExpected, theKeyChain );
	}

	@Test
	public void testChaosKeyChainBytes2() {
		final byte[] theExpected = new byte[] { 63, -16, 0, 0, 0, 0, 0, 0, 64, 16, 0, 0, 0, 0, 0, 0, 31, -1, -1, -1, -1, -1, -1, 0, 0 };
		final ChaosKey theKey = new ChaosKey( ChaosKey.X_MAX, ChaosKey.A_MAX, ChaosKey.S_MAX );
		final byte[] theKeyChain = theKey.toEncodedChain();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Key = " + Arrays.toString( theKey.getEncoded() ) );
			System.out.println( "Key chain = " + Arrays.toString( theKeyChain ) );
		}
		assertArrayEquals( theExpected, theKeyChain );
	}

	@Test
	public void testChaosKeyChainBytes3() {
		final ChaosKey theKey = ChaosKey.createRndKey( ChaosKey.createRndKey( ChaosKey.createRndKey() ) );
		final byte[] theKeyChain = theKey.toEncodedChain();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( ".........Key1 = " + Arrays.toString( theKey.getChild().getChild().getEncoded() ) );
			System.out.println( ".........Key2 = " + Arrays.toString( theKey.getChild().getEncoded() ) );
			System.out.println( ".........Key3 = " + Arrays.toString( theKey.getEncoded() ) );
			System.out.println( "....Key chain = " + Arrays.toString( theKeyChain ) );
		}
		final ChaosKey theNewKey = ChaosKey.createKeyChain( theKeyChain );
		final byte[] theNewKeyChain = theNewKey.toEncodedChain();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( ".....New key1 = " + Arrays.toString( theNewKey.getChild().getChild().getEncoded() ) );
			System.out.println( ".....New key2 = " + Arrays.toString( theNewKey.getChild().getEncoded() ) );
			System.out.println( ".....New key3 = " + Arrays.toString( theNewKey.getEncoded() ) );
			System.out.println( "New key chain = " + Arrays.toString( theNewKeyChain ) );
		}
		assertEquals( theNewKey, theKey );
		assertArrayEquals( theKeyChain, theNewKeyChain );
	}

	@Test
	public void testChaosKeyBytes1() {
		final ChaosKey theKey = new ChaosKey( ChaosKey.X_MIN, ChaosKey.A_MIN, ChaosKey.S_MIN );
		final byte[] theBytes = theKey.getEncoded();
		final ChaosKey theKey2 = new ChaosKey( theBytes );
		assertEquals( theKey2, theKey );
	}

	@Test
	public void testChaosKeyBytes2() {
		final ChaosKey theKey = new ChaosKey( ChaosKey.X_MAX, ChaosKey.A_MAX, ChaosKey.S_MAX );
		final byte[] theBytes = theKey.getEncoded();
		final ChaosKey theKey2 = new ChaosKey( theBytes );
		assertEquals( theKey2, theKey );
	}

	@Test
	public void testChaosKeyBytes3() {
		final ChaosKey theKey = new ChaosKey( ( ChaosKey.X_MIN + ChaosKey.X_MAX ) / 2, ( ChaosKey.A_MIN + ChaosKey.A_MAX ) / 2, ChaosKey.S_POSITIVE_MIN );
		final byte[] theBytes = theKey.getEncoded();
		final ChaosKey theKey2 = new ChaosKey( theBytes );
		assertEquals( theKey2, theKey );
	}

	@Test
	public void testChaosKeyBytes4() {
		final ChaosKey theKey = new ChaosKey( 0.1, 3.6, ChaosKey.S_NEGATIVE_MAX );
		final byte[] theBytes = theKey.getEncoded();
		final ChaosKey theKey2 = new ChaosKey( theBytes );
		assertEquals( theKey2, theKey );
	}

	@Test
	public void testX0ByInt() {
		testX0ByLong( Long.MIN_VALUE, 0 );
		testX0ByLong( 0, 0.5 );
		testX0ByLong( Long.MAX_VALUE, 1 );
	}

	@Test
	public void testAByInt() {
		testAByLong( Long.MIN_VALUE, 3.57 );
		testAByLong( 0, 3.785 );
		testAByLong( Long.MAX_VALUE, 4 );
	}

	@Test
	public void testSByInt() {
		testSByLong( ChaosKey.S_MIN, ChaosKey.S_MIN );
		testSByLong( ChaosKey.S_MIN - 1, -33 );
		testSByLong( 0, ChaosKey.S_NEGATIVE_MAX ); // -15 .. 15 are invalid as their S range is < 256!
		testSByLong( 15, ChaosKey.S_POSITIVE_MIN + 15 ); // -15 .. 15 are invalid as their S range is < 256!
		testSByLong( -15, ChaosKey.S_NEGATIVE_MAX - 15 ); // -15 .. 15 are invalid as their S range is < 256!
		testSByLong( 16, 16 ); // -16 .. 16 are invalid as their S range is < 256!
		testSByLong( 17, 17 );
		testSByLong( ChaosKey.S_MAX, ChaosKey.S_MAX );
		testSByLong( ChaosKey.S_MAX + 1, 33 );
	}

	@Test
	public void testAddToS() {
		testAddToS( 32, -20, -20 );
		testAddToS( 34, -38, -36 );
		testAddToS( -32, 20, 20 );
		testAddToS( -34, 38, 36 );
		testAddToS( ChaosKey.S_MAX, 127, ChaosKey.S_MIN + 127 );
		testAddToS( ChaosKey.S_MIN, -127, ChaosKey.S_MAX - 127 );
		testAddToS( 17, 1, 18 );
		testAddToS( -17, -1, -18 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void testAByLong( long aA, double aExpected ) {
		final double theA = ChaosKey.toA( aA );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aA + " --> to A: --> " + theA );
		}
		assertEquals( aExpected, theA );
	}

	private void testSByLong( long aS, long aExpected ) {
		final long theS = ChaosKey.toS( aS );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aS + " --> to S: --> " + theS );
		}
		assertEquals( aExpected, theS );
	}

	private void testX0ByLong( long x0, double aExpected ) {
		final double theX0 = ChaosKey.toX0( x0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( x0 + " --> to X0: --> " + theX0 );
		}
		assertEquals( aExpected, theX0 );
	}

	private void testAddToS( long aS, long aAddend, long aExpected ) {
		final long theS = ChaosKey.addToS( aS, aAddend );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "(" + aS + " + " + aAddend + ") --> add to S: --> " + theS );
		}
		assertEquals( aExpected, theS );
	}
}
